# -*- coding: utf-8 -*-
"""
Created on Thu Apr 26 13:08:49 2018

@author: Dipta Das
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#1.Import Dataset
dataset = pd.read_csv('Salary_Data.csv')
X = dataset.iloc[:,:-1].values
y = dataset.iloc[:,1].values

#4.Spliting the dataset into Training and Test Set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X,y, test_size = 1/3, random_state = 0)

#Fit Simple linear regression model to The Trainging set
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train, y_train)

#Predicting the Test set Result
y_pred =  regressor.predict(X_test)

#Visualising the Trainging Set results
plt.scatter(X_train, y_train, color='red')
plt.plot(X_train, regressor.predict(X_train), color = 'blue')
plt.title('Salary vs Evperiance(Training Set)')
plt.xlabel('Years of Experiance')
plt.ylabel('Salary')
plt.show()